import './styles.css';
import {Link} from "react-router-dom";
import React from 'react';

let textInput = React.createRef();

const Login = props => {

    function submitName() {
        localStorage.setItem('username', (textInput.current.value) ? textInput.current?.value : "Visitante");
    }

    return (
        <div className="container">
            <header className="Login-header">
                <h1>Pokedex</h1>
                <div className='captureBall'>
                    <input type='text' placeholder='Nome do treinador' ref={textInput} className="nameInput"/>
                    <Link to={'/pokedex'}>
                        <button className="buttonInput" onClick={submitName}>Ligar Pokedex</button>
                    </Link>
                </div>
            </header>
        </div>
    );
}

export default Login;
