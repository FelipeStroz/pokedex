import styled, {keyframes, css} from "styled-components";
import {fadeInRight} from "react-animations";
import Card from '@material-ui/core/Card';
import Grid from "@material-ui/core/Grid";

const enterAnimationList = keyframes`${fadeInRight}`;

export const Container = styled.div`
    overflow-x: hidden;
    display: flex;
    flex-direction: row;
    background-color: #f8f7f7;
    @media (max-width: 768px){
        margin-bottom: 0;
    }
`;

export const ContainerPokemons = styled(Grid)`
    padding: 0 1em 0 0;
    overflow-y: scroll;
    height: 100vh;
    flex: 1;
`;

export const CardPokemon = styled(Card)`
    animation: 1.5s ${enterAnimationList};
    display: flex;
    justify-content: flex-start;
    flex-direction: row;
    height: 60vh;
    border-radius: 0 15px 15px 0 !important;
    margin-top: 15px;
    background: linear-gradient(to bottom,#f1f0ef , #f7f6f6) !important;
`;

export const PokemonImage = styled.img`
    height: 14vh;
`;

export const SideBar = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;
    align-items: center;
    justify-content: center;
    width: 8em;
`

export const InformationDiv = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    border-radius: 35px;
    flex-direction: row;
    background-color: white;
    box-shadow: 0 0 30px rgba(0,0,0,0.1);
    width: 35vw;
    height: 50vh;
    margin-left: 5em;
`;

export const Number = styled.div`
   
`;

export const ContainerInformation = styled.div`
    display: flex;
    align-items: center;    
    width: 100%;
    
`;

export const HeightInformation = styled.div`
    margin: -1em;
    height: 90px;
    width: 90px;
    border-radius: 90px;
    display: flex;
    box-shadow: 0 0 30px rgba(0,0,0,0.07);
    justify-content: center;
    align-items: center;
    background-color: white;
    font-size: 1.1em;
`;

export const PrimaryInformation = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    margin-top: 4em;
`;

export const WeightInformation = styled.div`
    margin: -1em;
    height: 90px;
    width: 90px;
    border-radius: 90px;
    display: flex;
    box-shadow: 0 0 30px rgba(0,0,0,0.07);
    justify-content: center;
    align-items: center;
    background-color: white;
    font-size: 1.1em;
`;

export const NamePokemon = styled.text`
    color: #67645d;
    font-size: 2.2em;
    font-family: Open Sans;
    font-weight: bold;
    margin-top: 10px;
`;

export const ClassificationPokemon = styled.text`
    color: #67645d;
    font-size: 1em;
    font-family: Open Sans;
    font-weight: bold;
`;

export const TypesPokemon = styled.text`
    ${props => props.type === "Grass" && css`
        color: green;
    `}
    ${props => props.type === "Poison" && css`
        color: #a345a2;
    `}
    ${props => props.type === "Fire" && css`
        color: #f18d45;
    `}
    ${props => props.type === "Flying" && css`
        color: #9095f0;
    `}
    ${props => props.type === "Water" && css`
        color: #2027ff2b;
    `}
    ${props => props.type === "Bug" && css`
        color: #008e0b4d;
    `}
    ${props => props.type === "Normal" && css`
        color: #88888854;
    `}
    ${props => props.type === "Electric" && css`
        color: #c29949;
    `}
    ${props => props.type === "Ground" && css`
        color: #e1b86e;
    `}
    ${props => props.type === "Fairy" && css`
        color: #ffe772;
    `}
    ${props => props.type === "Fighting" && css`
        color: #c03027;
    `}
    ${props => props.type === "Psychic" && css`
        color: #ec508d;
    `}
    ${props => props.type === "Rock" && css`
        color: #b79f37;
    `}
    ${props => props.type === "Steel" && css`
        color: #7394a0;
    `}
    ${props => props.type === "Ice" && css`
        color: #9edad9;
    `}
    ${props => props.type === "Dragon" && css`
        color: #743ef8;
    `}
    ${props => props.hover && css`
        &:hover{
            color: white;
            cursor: pointer;
        }
    `}
    font-size: ${props => props.size}em;
    font-family: cursive;
`;

export const DivTypes = styled.div`
   display: flex;
   flex-direction: row;
   margin-top: 20px;
`;

export const ContainerOtherInformation = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    flex: 1;
    height: 100%;
    padding: 0 3em 0 0;
`;

export const DivProgressBar = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: space-between;
    align-items: center;
`;

export const InformationText = styled.text`
    color: #67645d;
    font-size: 1.2em;
    font-family: Open Sans;
    font-weight: bold;
    margin: 12px;
`;

export const DivHpCp = styled.div`
    display: flex;
    align-items: center;
    flex-direction: row;
    flex: 1;
    width: 100%;
`;

export const Buttons = styled.div`
    margin: 0 5em;
    width: 90px;
    height: 40px;
    background-color: #a5a5a5;
    border-radius: 25px;
    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 0 0 30px rgba(0,0,0,0.07);
    &:hover {
        background-color: #ff8282;
        cursor: pointer;
    }
`;

export const DivButtons = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    height: 30%;
    width: 100%;
`;

export const Username = styled.text`
    font-size: 1.4em;
    color: white;
    margin-bottom: 10px;
    font-family: cursive;
`;

export const SideBarTypes = styled.div`
   display: flex;
   align-items: center;
   justify-content: center;
   flex-direction: row;
   width: 90%;
   border-radius: 10px;
   margin-top: 5px;
   height: 43px;
   background-color: #d5d5d5c2;
   &:hover {
    ${props => props.type === "Grass" && css`
        background-color: green;
    `}
    ${props => props.type === "Poison" && css`
        background-color: #a345a2;
    `}
    ${props => props.type === "Fire" && css`
        background-color: #f18d45;
    `}
    ${props => props.type === "Flying" && css`
        background-color: #9095f0;
    `}
    ${props => props.type === "Water" && css`
        background-color: #2027ff2b;
    `}
    ${props => props.type === "Bug" && css`
        background-color: #008e0b4d;
    `}
    ${props => props.type === "Normal" && css`
        background-color: #88888854;
    `}
    ${props => props.type === "Electric" && css`
        background-color: #c29949;
    `}
    ${props => props.type === "Ground" && css`
        background-color: #e1b86e;
    `}
    ${props => props.type === "Fairy" && css`
        background-color: #ffe772;
    `}
    ${props => props.type === "Fighting" && css`
        background-color: #c03027;
    `}
    ${props => props.type === "Psychic" && css`
        background-color: #ec508d;
    `}
    ${props => props.type === "Rock" && css`
        background-color: #b79f37;
    `}
    ${props => props.type === "Steel" && css`
        background-color: #7394a0;
    `}
    ${props => props.type === "Ice" && css`
        background-color: #9edad9;
    `}
    ${props => props.type === "Dragon" && css`
        background-color: #743ef8;
    `}
   }
`;

export const Header = styled.div`
    width: 100%;
    height: 5em;
    background-color: red;
    display: flex;
    align-items: flex-start;
    padding: 1em;
    flex-direction: column;
    position: fixed;
`;
