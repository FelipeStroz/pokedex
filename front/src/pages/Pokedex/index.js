import React, {useEffect, useState} from "react";
import getListPokemons from "../../services/getListPokemons";
import ReactLoading from "react-loading";
import ProgressBar from "../../components/ProgressBar";
import {Grid} from "@material-ui/core";
import {
    InformationDiv,
    CardPokemon,
    Container,
    PokemonImage,
    SideBar,
    ContainerPokemons,
    Number,
    ContainerInformation,
    HeightInformation,
    PrimaryInformation,
    WeightInformation,
    NamePokemon,
    ClassificationPokemon,
    TypesPokemon,
    DivTypes,
    ContainerOtherInformation,
    DivProgressBar,
    InformationText,
    Buttons,
    DivHpCp,
    DivButtons,
    Username,
    SideBarTypes,
    Header,
} from "./styles";

let initList = [{
    name: <ReactLoading type="spin" color="#fff" width={200} height={30}/>,
    number: <ReactLoading type="spin" color="#fff" width={200} height={30}/>,
    classification: <ReactLoading type="spin" color="#fff" width={200} height={30}/>,
    image: <ReactLoading type="spin" color="#fff" width={200} height={30}/>,
    types: <ReactLoading type="spin" color="#fff" width={200} height={30}/>,
}]

let oldFilter = 0;

export default function Pokedex() {
    let [isLoading, setIsLoading] = useState(true);
    let [error, setError] = useState(false);
    let [list, setList] = useState(initList);
    let [searchList, setSearchList] = useState(initList);
    let [type, setType] = useState(null);
    let username = localStorage.getItem("username");

    useEffect(() => {
        setIsLoading(true);
        getListPokemons().then((data) => {
            setList(data.pokemons);
            setSearchList(data.pokemons);
            setIsLoading(false);
        }).catch(e => {
            setError(e);
            setIsLoading(false);
        });
    }, []);

    let types = [
        "Grass",
        "Poison",
        "Fire",
        "Flying",
        "Water",
        "Bug",
        "Normal",
        "Electric",
        "Ground",
        "Fairy",
        "Fighting",
        "Psychic",
        "Rock",
        "Steel",
        "Ice",
        "Dragon",
    ];
    let input = React.createRef();
    let selectedType = React.createRef();

    const filterItems = () => {
        if (input.current?.value) {
            if (oldFilter > input.current?.value.length)
                list = searchList;
            oldFilter = input.current?.value.length;
            setList(list?.filter(el => el.name.toLowerCase().indexOf(input.current?.value.toLowerCase()) > -1));
        }else{
            setList(searchList);
            oldFilter = 0;
        }
    };

    const filterTypes = () => {
        // console.log(selectedType.current.innerText);
        // setType(type);
        let array = [];
        for (let position of list){
            for(let type of position.types)
                if(type === selectedType.current.innerText)
                    array.push(position);
        }
        setList(array);
    };

    return (
        <Container>
            <SideBar>
                {types.map((type) =>
                    <SideBarTypes onClick={filterTypes} ref={selectedType} key={type} type={type}>
                        <TypesPokemon hover={true} size={1} type={type}>{type}</TypesPokemon>
                    </SideBarTypes>
                )}
            </SideBar>
            {!isLoading ?
                <ContainerPokemons container>
                    <Header>
                        <Username>Treinador {username}</Username>
                        <input placeholder={"Search Pokemon..."} ref={input} onChange={filterItems}/>
                    </Header>
                    <div style={{marginTop: "6em", width: "100%"}}>
                    {
                        (!list[0])? <h1>None Pokemon founded!</h1> :
                        list.map(({name, number, classification, image, maxCP, maxHP, types, fleeRate, height, weight}) =>

                            <Grid item xs={12} spacing={2}>
                                <CardPokemon>
                                    <Number>#{number}</Number>
                                    <ContainerInformation>
                                        <InformationDiv>
                                            <HeightInformation>
                                                <strong>{height?.maximum}</strong>
                                            </HeightInformation>
                                            <PrimaryInformation>
                                                <PokemonImage src={image}/>
                                                <NamePokemon>{name}</NamePokemon>
                                                <ClassificationPokemon>{classification}</ClassificationPokemon>
                                                <DivTypes>
                                                    {types?.map((type) => (types.indexOf(type) === 1) ?
                                                        <div>●<TypesPokemon hover={false} size={1.7}
                                                                            type={type}>{type}</TypesPokemon>
                                                        </div> :
                                                        <TypesPokemon hover={false} size={1.7}
                                                                      type={type}>{type}</TypesPokemon>)}
                                                </DivTypes>
                                                <div><strong>Flee rate:</strong> {fleeRate}</div>
                                            </PrimaryInformation>
                                            <WeightInformation>
                                                <strong>{weight?.maximum}</strong>
                                            </WeightInformation>
                                        </InformationDiv>
                                        <ContainerOtherInformation>
                                            <DivHpCp>
                                                <DivProgressBar>
                                                    <InformationText>HP</InformationText>
                                                    <InformationText>CP</InformationText>
                                                </DivProgressBar>
                                                <DivProgressBar>
                                                    <InformationText>{maxHP}</InformationText>
                                                    <InformationText>{maxCP}</InformationText>
                                                </DivProgressBar>
                                                <DivProgressBar>
                                                    <ProgressBar completed={maxHP}/>
                                                    <ProgressBar completed={maxCP}/>
                                                </DivProgressBar>
                                            </DivHpCp>
                                            <DivButtons>
                                                <Buttons>Attacks</Buttons>
                                                <Buttons>More Info...</Buttons>
                                            </DivButtons>
                                        </ContainerOtherInformation>
                                    </ContainerInformation>

                                </CardPokemon>
                            </Grid>)}</div>
                </ContainerPokemons>
                : <ReactLoading type="spin" color="black" width={200} height={30}/>}
        </Container>

    )
}
