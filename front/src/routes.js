import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
// import Home from "./pages/Home";
// import AdminPage from "./pages/Admin";
// import SelectGroup from "./pages/SelectGroup";
// import Login from "./pages/Login";
// import Result from "./pages/Result";
import Login from "./pages/Login";
import Pokedex from "./pages/Pokedex";

export default function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path={'/'}>
                    <Login />
                </Route>
                <Route exact path={'/pokedex'}>
                    <Pokedex />
                </Route>
                <Route path="/">
                    Não encontrado 404
                </Route>
            </Switch>
        </Router>
    )
}
