import styled from "styled-components";

export const Container = styled.div`
    height: 20px;
    width: 100%;
    background-color: #e9e8e7;
    border-radius: 50px;
    margin: 12px;
`;

export const Fill = styled.div`
    height: 100%;
    width: ${props => props.completed}%;
    background: ${props => props.color};
    border-radius: inherit;
    text-align: right;
`;

export const Label = styled.span`
    padding: 5;
    color: white;
    font-weight: bold;
`;
