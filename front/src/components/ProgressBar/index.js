import React from "react";
import {Container, Fill, Label} from "./styles";

const ProgressBar = (props) => {
    const { completed } = props;
    let percent = (completed/4144)*100, color;
    percent = percent.toFixed(0);
    if (percent > 70)
        color = "linear-gradient(to right, #79af60 , #5b9d3a)"
    else if (percent > 50)
        color = "linear-gradient(to right, #f7d04c , #f5c304)"
    else if (percent > 30)
        color = "linear-gradient(to right, #f6ac48 , #f5940a)"
    else if (percent <= 30)
        color = "linear-gradient(to right, #e36262 , #dd403f)"

    return (
        <Container>
            <Fill color={color} completed={percent}>
                <Label />
            </Fill>
        </Container>
    );
};

export default ProgressBar;
