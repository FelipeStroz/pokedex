export default async function getPokemons() {

    let url = `${process.env.REACT_APP_API_HOST}`
    let body = "{\"query\":\"{\\n  pokemons(first: 200) {\\n    id\\n    number\\n    name\\n    types\\n    attacks{\\n      fast{\\n        name\\n        type\\n        damage\\n      }\\n      special{\\n        name\\n        type\\n        damage\\n      }\\n    }\\n   resistant\\n   fleeRate\\n   weaknesses\\n   classification\\n   evolutions{\\n    number\\n    name\\n    types\\n    attacks{\\n      fast{\\n        name\\n        type\\n        damage\\n      }\\n      special{\\n        name\\n        type\\n        damage\\n      }\\n    }\\n   resistant\\n   fleeRate\\n   weaknesses\\n   classification\\n    weight{\\n    minimum\\n    maximum\\n  }\\n    height{\\n    minimum\\n    maximum\\n  }\\n   image\\n  }\\n  evolutionRequirements{\\n    amount\\n    name\\n  }\\n  weight{\\n    minimum\\n    maximum\\n  }\\n    height{\\n    minimum\\n    maximum\\n  }\\n    maxCP\\n    maxHP\\n    image\\n    \\n  }\\n}\\n\"}";
    let headers = {'Content-Type': "application/json"};

    let response = await fetch(url, {method: 'POST', headers, body}).catch((e)=> console.log(e));

    if (response.status !== 200) return Promise.reject(await response.json());

    let json;

    try {
        json = await response.json();
    } catch (e) {
        return Promise.reject('Erro desconhecido, tente novamente mais tarde.');
    }

    return json.data;
}
