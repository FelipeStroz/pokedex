let express = require('express');
let app = express();
require('dotenv').config();
// const https = require('https');
// const fs = require('fs');
const path = require('path');

app.use(express.static(__dirname + '/build'));

app.get('*', function (request, response){
    response.sendFile(path.resolve(__dirname, 'build', 'index.html'))
});

// let isHttps = process.env.HTTPS === 'true';
//
// if (isHttps)
//     https.createServer({
//         key: fs.readFileSync(process.env.KEY),
//         cert: fs.readFileSync(process.env.CERT),
//         passphrase: process.env.PASSPHRASE
//     }, app).listen(process.env.PORT, function () {
//         console.log(`Servidor de produção iniciando na porta ${process.env.PORT} [HTTPS]`);
//     });

app.listen(process.env.PORT, function () {
    console.log(`Servidor de produção iniciando na porta ${process.env.PORT} [SEM HTTPS]`);
});
